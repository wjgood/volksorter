﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volksorter_Interface
{
    public enum mode { Sort, Data, Blue, Green, Red, IR850, IR880, IR910, IR940, IR970, IR1070 }

    /// <summary>
    /// Main Controller for the program. Handles data between the device and the user interface
    /// </summary>
    public class Controller
    {
        private mainForm view;
        private SorterComm sorter;
        private readonly int BUFFER_SIZE_SORT = 7;
        private readonly int BUFFER_SIZE_DATA = 44;
        private int inputBufferSizeAccepted;
        private Queue<byte> inputBufferQueue;
        private mode currentMode;
        private enum save { None, Dark, Light, Other}
        private save saveMode = save.None;
        private StreamWriter outputWriter;
        private int counter;
        private string sampleOutputFile;
        private int sampleSize;
        private bool includeDiscrim;

        /// <summary>
        /// Constructor to create an instance of the controller
        /// </summary>
        /// <param name="_view"></param>
        public Controller(mainForm _view)
        {
            view = _view;
            inputBufferQueue = new Queue<byte>();
            sorter = new SorterComm(sorterListener);
            changeModeTo(mode.Sort);
        }

        public mainForm View { get => view; set => view = value; }

        /// <summary>
        /// Changes to the given mode
        /// </summary>
        /// <param name="m"></param>
        public void changeModeTo(mode m)
        {
            if (sorter != null && sorter.IsConnected)
            {
                currentMode = m;
                sorter.changeModeTo(m);
                switch (m)
                {
                    case mode.Sort:
                        inputBufferSizeAccepted = BUFFER_SIZE_SORT;
                        counter = 0;
                        break;
                    case mode.Data:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.Blue:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.Green:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.Red:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.IR850:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.IR880:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.IR910:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.IR940:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.IR970:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                    case mode.IR1070:
                        inputBufferSizeAccepted = BUFFER_SIZE_DATA;
                        break;
                }

                printLine("Set to receive " + inputBufferSizeAccepted + " bytes.");
            }
        }

        public void changeModeTo(string m)
        {
            switch (m)
            {
                case "rbSort": changeModeTo(mode.Sort); break;
                case "rbData": changeModeTo(mode.Data); break;
                case "rbBlue": changeModeTo(mode.Blue); break;
                case "rbGreen": changeModeTo(mode.Green); break;
                case "rbRed": changeModeTo(mode.Red); break;
                case "rbIR850": changeModeTo(mode.IR850); break;
                case "rbIR880": changeModeTo(mode.IR880); break;
                case "rbIR910": changeModeTo(mode.IR910); break;
                case "rbIR940": changeModeTo(mode.IR940); break;
                case "rbIR970": changeModeTo(mode.IR970); break;
                case "rbIR1070": changeModeTo(mode.IR1070); break;
            }
        }

        /// <summary>
        /// Receives data or information from the SorterComm object
        /// </summary>
        /// <param name="tag">  "i" information. "d" data </param>
        /// <param name="o"></param>
        public void sorterListener(char tag, object o)
        {
            switch (tag)
            {
                case 'i':
                    if (o is string)
                        printLine((string)o);
                    break;
                case 'd':
                    if (o is string)
                        printLine((string)o);
                    else if (o is byte[])
                    {
                        byte[] byteArray = (byte[])o;
                        foreach (byte b in byteArray)
                            inputBufferQueue.Enqueue(b);
                        if (inputBufferQueue.Count >= inputBufferSizeAccepted)
                            doThingsWithBuffer();
                    }
                    break;
            }
        }

        /// <summary>
        /// Passes a string to the user interface to print
        /// </summary>
        /// <param name="s"></param>
        private void printLine(string s)
        {
            view.printLine(s);
        }

        /// <summary>
        /// The input buffer is full. This processes the input based on the mode.
        /// </summary>
        private void doThingsWithBuffer()
        {
            counter++;
            byte[] tempBuffer = new byte[inputBufferSizeAccepted];

            for (int i = 0; i < inputBufferSizeAccepted; i++) 
            {
                tempBuffer[i] = inputBufferQueue.Dequeue();
            }

            if (currentMode == mode.Sort)
            {
                int discrim = tempBuffer[1] * 256 + tempBuffer[0];
                int rejectThresh = tempBuffer[2];
                int kernelCount = tempBuffer[4] * 256 + tempBuffer[3];
                int rejectCount = tempBuffer[6] * 256 + tempBuffer[5];
                printLine(counter + ",    " + discrim + ", " + rejectThresh + ", " + kernelCount + ", " + rejectCount);

                view.updateCounts(kernelCount, rejectCount);
            }
            else
            {
                StringBuilder output = new StringBuilder();
                output.Append(counter);

                //tempBuffer 0-33, combine pairs of bytes and print, do things save mode
                int[] idata = new int[17];
                int x = 0;
                for (int i = 0; i <= 33; i += 2) 
                {
                    idata[x] = tempBuffer[i + 1] * 256 + tempBuffer[i];
                    output.Append(", ");
                    output.Append(idata[x]);
                    x++;
                }

                //tempBuffer 34, reject thresh
                output.Append(", ");
                output.Append(tempBuffer[34]);

                //tempBuffer 35-43, print triples of bytes : a b c, d e f,
                for (int i = 35; i <= 43; i += 3)
                {
                    output.Append(", ");
                    output.Append(tempBuffer[i]);
                    output.Append(" ");
                    output.Append(tempBuffer[i+1]);
                    output.Append(" ");
                    output.Append(tempBuffer[i+2]);
                }
                printLine(output.ToString());

                if (saveMode != save.None)
                {
                    if (counter == sampleSize)
                    {
                        printLine("-----------------------");
                        printLine("Data for " + sampleSize + " kernels complete. Switch kernel type.");
                        printLine("-----------------------");
                    }
                    if (counter < (sampleSize+4))
                    {
                        StringBuilder saveString = new StringBuilder();
                        saveString.Append(counter);
                        saveString.Append(",");
                        saveString.Append(inputBufferSizeAccepted);
                        saveString.Append(",");
                        saveString.Append( ((FileStream)outputWriter.BaseStream).Name.Split('\\').Last()[0] ); //get first character of open file
                        saveString.Append(",");

                        for (int i = 0; i <= 11; i++)
                        {
                            saveString.Append(idata[i]); saveString.Append(",");
                        }

                        if (includeDiscrim)
                        {
                            for (int i = 1; i <= 10; i++)
                            {
                                for (int j = i + 1; j <= 11; j++)
                                {
                                    saveString.Append(idata[i] - idata[j]);
                                    saveString.Append(",");
                                }
                            }

                            for (int i = 1; i <= 10; i++)
                            {
                                for (int j = i + 1; j <= 11; j++)
                                {
                                    saveString.Append((256 * idata[i]) / (idata[j] + 256));
                                    saveString.Append(",");
                                }
                            }

                            for (int i = 1; i <= 7; i++)
                            {
                                for (int j = i + 1; j <= 8; j++)
                                {
                                    for (int k = j + 1; k <= 9; k++)
                                    {
                                        saveString.Append(idata[i] - (2 * idata[j]) + idata[k]);
                                        saveString.Append(",");
                                    }
                                }
                            }
                        }
                        
                        saveString.Append("77");
                        outputWriter.WriteLine(saveString.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Changes the reject threshold of the device
        /// </summary>
        /// <param name="rt"></param>
        public void changeRejectThreshold(int rt)
        {
            sorter.sendRejectThreshold(rt);
        }

        /// <summary>
        /// Resets the count and sets the device to Sort Mode
        /// </summary>
        public void resetDevice()
        {
            changeModeTo(mode.Data);
            changeModeTo(mode.Sort);
        }

        /// <summary>
        /// Opens the calibration file and sends the data to the SorterComm object
        /// </summary>
        /// <param name="filename"></param>
        private void loadCalibrationFile(string filename)
        {
            loadCalibrationFile(filename, view.getSeedSize());
        }

        /// <summary>
        /// Opens the calibration file and sends the data to the SorterComm object
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="seed"></param>
        public void loadCalibrationFile(string filename, int seed)
        {
            try
            {
                if (File.Exists(filename))
                {
                    string calFileAll = File.ReadAllText(filename); 
                    string[] calFileLines = calFileAll.Split('\n'); 

                    if (calFileLines.Count() == 8)
                    {
                        string temp1 = calFileLines[0].Split('=').Last();
                        string temp2 = temp1.Split(';')[0];
                        int discrim1 = Convert.ToInt32(temp2);
                        
                        temp1 = calFileLines[1].Split('=').Last();
                        temp2 = temp1.Split(';')[0];
                        int discrim2 = Convert.ToInt32(temp2);

                        temp1 = calFileLines[2].Split('=').Last();
                        temp2 = temp1.Split(';')[0];
                        int discrim3 = Convert.ToInt32(temp2);
                        
                        temp1 = calFileLines[3].Split('=').Last();
                        temp2 = temp1.Split(';')[0];
                        int discrimConst = Convert.ToInt32(temp2);

                        temp1 = calFileLines[4].Split('=').Last();
                        temp2 = temp1.Split(';')[0];
                        int f1 = Convert.ToInt32(temp2);

                        temp1 = calFileLines[5].Split('=').Last();
                        temp2 = temp1.Split(';')[0];
                        int f2 = Convert.ToInt32(temp2);

                        temp1 = calFileLines[6].Split('=').Last();
                        temp2 = temp1.Split(';')[0];
                        int f3 = Convert.ToInt32(temp2);

                        temp1 = calFileLines[7].Split('=').Last();
                        temp2 = temp1.Split(';')[0];
                        int signs = Convert.ToInt32(temp2);

                        sorter.sendCalibration(discrim1, discrim2, discrim3, discrimConst, f1, f2, f3, signs, seed);
                    }
                    else
                    {
                        printLine("ERROR: Calibration file is not properly formatted.");
                    }
                }
                else
                {
                    //file does not exist
                    printLine("ERROR: Calibration file does not exist. Refresh the list");
                }
            }
            catch (Exception ex)
            {
                printLine("ERROR: There was a problem reading the calibration file.");
                printLine(ex.Message);
            }
        }

        /// <summary>
        /// Sets program to save the Dark file. 
        /// </summary>
        public void saveDark()
        {
            saveMode = save.Dark;
            counter = 0;
            sampleSize = 201;
            includeDiscrim = true;
            outputWriter = new StreamWriter("dark.csv");
            printLine("Set to collect dark kernel data.");
        }

        /// <summary>
        /// Closes the Dark.csv file
        /// </summary>
        public void closeDark()
        {
            outputWriter.Close();
            saveMode = save.None;
            printLine("Data file closed.");
        }
        
        /// <summary>
        /// Sets the program to save the Light file
        /// </summary>
        public void saveLight()
        {
            saveMode = save.Light;
            counter = 0;
            sampleSize = 201;
            includeDiscrim = true;
            outputWriter = new StreamWriter("light.csv");
            printLine("Set to collect light kernel data.");
        }

        /// <summary>
        /// Closes the Light.csv file
        /// </summary>
        public void closeLight()
        {
            outputWriter.Close();
            saveMode = save.None;
            printLine("Data file closed.");
        }

        /// <summary>
        /// Creates the Reject.csv and Accept.csv files and runs Tom's discrim program
        /// </summary>
        /// <param name="rejectDark"></param>
        public void createCalibration(bool rejectDark)
        {
            if (saveMode == save.None)
            {
                try
                {
                    if (rejectDark)
                    {
                        File.Copy("dark.csv", "reject.csv", true);
                        File.Copy("light.csv", "accept.csv", true);
                    }
                    else
                    {
                        File.Copy("light.csv", "reject.csv", true);
                        File.Copy("dark.csv", "accept.csv", true);
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show("Dark.csv or Light.csv are missing or corrupted./n" + ex.Message);
                }

                try
                {
                    using (Process exeProcess = Process.Start("VolkSortdisc3.exe"))
                    {
                        exeProcess.WaitForExit();
                        //loadCalibrationFile("function.cal");
                    }
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("VolkSortdisc3.exe not found or is corrupted.");
                }
            }
        }

        /// <summary>
        /// Resaves the function.cal file to the new name.
        /// </summary>
        /// <param name="newFilename"></param>
        public void resaveCalibration(string newFilename)
        {
            if (File.Exists("function.cal"))
            {
                File.Copy("function.cal", newFilename, true);
                view.updateCalFileList();
            }
        }

        /// <summary>
        /// Connects to reconnects to the device
        /// </summary>
        public void reconnect()
        {
            sorter.findAndConnect();
            changeModeTo(mode.Sort);
        }

        /// <summary>
        /// Disconnects from the device
        /// </summary>
        public void disconnect()
        {
            sorter.disconnect();
        }

        /// <summary>
        /// Sets the program to save a file with the given name
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="_sampleSize"></param>
        /// <param name="_includeDiscrim"></param>
        public void saveSample(string filename, int _sampleSize, bool _includeDiscrim)
        {
            sampleOutputFile = filename;
            saveMode = save.Other;
            counter = 0;
            sampleSize = _sampleSize;
            includeDiscrim = _includeDiscrim;
            outputWriter = new StreamWriter(filename);
            printLine("Set to collect sample kernel data into " + filename);
        }

        /// <summary>
        /// Closes the currently open sample file
        /// </summary>
        public void closeSample()
        {
            outputWriter.Close();
            saveMode = save.None;
            printLine("Data file closed.");
        }

        /// <summary>
        /// Fires the air valve
        /// </summary>
        public void testAir()
        {
            sorter.testAir();
        }
    }
}
