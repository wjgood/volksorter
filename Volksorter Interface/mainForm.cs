﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Volksorter_Interface
{
    public partial class mainForm : Form
    {
        private Controller controller;

        /// <summary>
        /// Constructor to create instance of the user interface
        /// </summary>
        public mainForm()
        {
            InitializeComponent();
            updateCalFileList();
        }

        /// <summary>
        /// Gets or sets a handle for the controller
        /// </summary>
        public Controller Controller { get => controller; set => controller = value; }

        /// <summary>
        /// Prints the text to the output text box.
        /// </summary>
        /// <param name="s"></param>
        public void printLine(string s)
        {
            if (tbOutput.InvokeRequired)
            {
                tbOutput.Invoke(new printLineDelegate(printLine), s);
            }
            else
            {
                tbOutput.AppendText("\r\n" + s);
            }
        }
        /// <summary>
        /// Delegates are required when the method is called from a different thread from the UI thread.
        /// </summary>
        /// <param name="s"></param>
        private delegate void printLineDelegate(string s);
        
        private void btConnect_Click(object sender, EventArgs e)
        {
            controller.reconnect();
        }

        private void btDisconnect_Click(object sender, EventArgs e)
        {
            controller.disconnect();
        }

        /// <summary>
        /// The user has selected a new mode (Sort, Data, or colors). Make changes to UI and send mode to Controller
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void modeChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                switch ((sender as RadioButton).Name)
                {
                    case "rbSort":
                        controller.changeModeTo(mode.Sort);
                        btCollectDark.Enabled = false;
                        btCollectLight.Enabled = false;
                        btCollectSample.Enabled = false;
                        btCloseDark.Enabled = false;
                        btCloseLight.Enabled = false;
                        break;
                    case "rbData":
                        controller.changeModeTo(mode.Data);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbBlue":
                        controller.changeModeTo(mode.Blue);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbGreen":
                        controller.changeModeTo(mode.Green);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbRed":
                        controller.changeModeTo(mode.Red);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbIR850":
                        controller.changeModeTo(mode.IR850);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbIR880":
                        controller.changeModeTo(mode.IR880);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbIR910":
                        controller.changeModeTo(mode.IR910);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbIR940":
                        controller.changeModeTo(mode.IR940);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbIR970":
                        controller.changeModeTo(mode.IR970);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                    case "rbIR1070":
                        controller.changeModeTo(mode.IR1070);
                        btCollectDark.Enabled = true;
                        btCollectLight.Enabled = true;
                        btCollectSample.Enabled = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Button to send the reject threshold has been clicked. Tell the controller.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btRejectThresh_Click(object sender, EventArgs e)
        {
            controller.changeRejectThreshold(Convert.ToInt32(nudRejectThresh.Value));
        }

        /// <summary>
        /// Update the labels that display the kernel and reject counts
        /// </summary>
        /// <param name="kernels"></param>
        /// <param name="rejects"></param>
        public void updateCounts(int kernels, int rejects)
        {
            if (lblKernelNum.InvokeRequired)
            {
                lblKernelNum.Invoke(new updateCountsDelegate(updateCounts), kernels, rejects);
            }
            else
            {
                lblKernelNum.Text = kernels.ToString();
                lblRejectNum.Text = rejects.ToString();
            }
        }
        /// <summary>
        /// Delegates are required when the method is called from a different thread from the UI thread.
        /// </summary>
        /// <param name="kernels"></param>
        /// <param name="rejects"></param>
        private delegate void updateCountsDelegate(int kernels, int rejects);

        /// <summary>
        /// Reset counts and switch between data and sort mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btResetCounts_Click(object sender, EventArgs e)
        {
            updateCounts(0, 0);
            //controller.resetDevice();
            rbData.Checked = true;
            rbSort.Checked = true;
        }
        
        private void btTestAir_Click(object sender, EventArgs e)
        {
            controller.testAir();
        }
        

        /// <summary>
        /// Updates the calibration file list
        /// </summary>
        public void updateCalFileList()
        {
            lbCalFileList.Items.Clear();
            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory());
            foreach (string f in files)
            {
                if (f.Split('.').Last() == "cal")
                {
                    string filename = f.Split('\\').Last();
                    lbCalFileList.Items.Add(filename);
                }
            }

            try
            {
                lbCalFileList.SetSelected(lbCalFileList.FindString("function.cal"), true);
            }
            catch
            {
                MessageBox.Show("function.cal file not found.");
            }
        }

        /// <summary>
        /// Call the method to update the calibration list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btCalFileRefresh_Click(object sender, EventArgs e)
        {
            updateCalFileList();
        }

        /// <summary>
        /// The user has selected a file. Update the label to show this.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbCalFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblSelectedCalFile.Text = lbCalFileList.SelectedItem.ToString();
        }

        /// <summary>
        /// Get the value of the selected seed size
        /// </summary>
        /// <returns></returns>
        public int getSeedSize()
        {
            int seedSize = 0;
            if (rbAlfalfa.Checked) seedSize = 8;
            else if (rbFlax.Checked) seedSize = 11;
            else if (rbWheat.Checked) seedSize = 12;
            else if (rbDurum.Checked) seedSize = 13;
            else if (rbBarley.Checked) seedSize = 16;
            else if (rbCorn.Checked) seedSize = 20;
            return seedSize;
        }
        

        private void btCollectDark_Click(object sender, EventArgs e)
        {
            btCollectDark.Enabled = false;
            btCollectLight.Enabled = false;
            btCollectSample.Enabled = false;
            btCreateCalibration.Enabled = false;

            btCloseDark.Enabled = true;

            controller.saveDark();
        }

        private void btCloseDark_Click(object sender, EventArgs e)
        {
            btCloseDark.Enabled = false;

            controller.closeDark();

            btCollectDark.Enabled = true;
            btCollectLight.Enabled = true;
            btCollectSample.Enabled = true;
            btCreateCalibration.Enabled = true;
        }


        private void btCollectLight_Click(object sender, EventArgs e)
        {
            btCollectDark.Enabled = false;
            btCollectLight.Enabled = false;
            btCollectSample.Enabled = false;
            btCreateCalibration.Enabled = false;

            btCloseLight.Enabled = true;

            controller.saveLight();
        }

        private void btCloseLight_Click(object sender, EventArgs e)
        {
            btCloseLight.Enabled = false;

            controller.closeLight();

            btCollectDark.Enabled = true;
            btCollectLight.Enabled = true;
            btCollectSample.Enabled = true;
            btCreateCalibration.Enabled = true;
        }


        private void btCollectSample_Click(object sender, EventArgs e)
        {

            btCollectDark.Enabled = false;
            btCollectLight.Enabled = false;
            btCollectSample.Enabled = false;
            btCreateCalibration.Enabled = false;

            btCloseSample.Enabled = true;

            string[] filename = tbSaveDataFileName.Text.Split('.');
            if ((filename.Length == 1) || (!filename.Last().Equals(".csv")))
                tbSaveDataFileName.Text += ".csv";

            controller.saveSample(tbSaveDataFileName.Text, (int)nupSampleSize.Value, cbIncludeDiscrim.Checked);
        }

        private void btCloseSample_Click(object sender, EventArgs e)
        {
            btCloseSample.Enabled = false;

            controller.closeSample();
            
            btCollectDark.Enabled = true;
            btCollectLight.Enabled = true;
            btCollectSample.Enabled = true;
            btCreateCalibration.Enabled = true;

        }


        private void btCreateCalibration_Click(object sender, EventArgs e)
        {
            btCollectDark.Enabled = false;
            btCollectLight.Enabled = false;
            btCreateCalibration.Enabled = false;

            controller.createCalibration(rbRejectDark.Checked);
            if (cbLoadCalAfterCreate.Checked)
            {
                int seedSize = 0;
                if (rbAlfalfa.Checked) seedSize = 8;
                else if (rbFlax.Checked) seedSize = 11;
                else if (rbWheat.Checked) seedSize = 12;
                else if (rbDurum.Checked) seedSize = 13;
                else if (rbBarley.Checked) seedSize = 16;
                else if (rbCorn.Checked) seedSize = 20;

                controller.loadCalibrationFile("function.cal", seedSize);
            }
            
            btCollectDark.Enabled = true;
            btCollectLight.Enabled = true;
            btCreateCalibration.Enabled = true;
        }

        private void btSaveCal_Click(object sender, EventArgs e)
        {
            controller.resaveCalibration(tbCalibrationName.Text);
        }

        /// <summary>
        /// Load Calibration button. Send the file to the controller.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btLoadCal_Click(object sender, EventArgs e)
        {
            if (lbCalFileList.SelectedItem != null)
            {
                lblLastCalLoaded.Text = lbCalFileList.SelectedItem.ToString();

                controller.loadCalibrationFile(lbCalFileList.SelectedItem.ToString(), getSeedSize());
            }
        }
        
        /// <summary>
        /// Draws a border around the given box.
        /// </summary>
        /// <param name="box"></param>
        /// <param name="g"></param>
        /// <param name="textColor"></param>
        /// <param name="borderColor"></param>
        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Clear text and border
                g.Clear(this.BackColor);

                // Draw text
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        /// <summary>
        /// This is called when each groupbox is painted on the interface and changes the border color to Black.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void groupbox_paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Black, Color.Black);
        }

    }
}
