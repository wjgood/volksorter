﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Management;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Volksorter_Interface
{
    //This class handles all the communication with the sorter device.
    class SorterComm
    {
        // Serial Comm Port defaults
        private SerialPort  comPort;
        private string      port_name           = "COM3";
        private int         port_BaudRate       = 115200;
        private Parity      port_Parity         = Parity.None;
        private int         port_DataBits       = 8;
        private StopBits    port_StopBits       = StopBits.One;
        private int         port_ReadTimeout    = 500;
        private int         port_WriteTimeout   = 500;
        private string      InputData           = String.Empty;

        // Commands for the sorter device
        private readonly byte cmdCALIBRATION = 222;
        private readonly byte cmdSORT        = 223;
        private readonly byte cmdDATA        = 224;
        private readonly byte cmdBLUE        = 225;
        private readonly byte cmdGREEN       = 226;
        private readonly byte cmdRED         = 227;
        private readonly byte cmdIR850       = 228;
        private readonly byte cmdIR880       = 229;
        private readonly byte cmdIR910       = 230;
        private readonly byte cmdIR940       = 231;
        private readonly byte cmdIR970       = 232;
        private readonly byte cmdIR1070      = 233;
        private readonly byte cmdAir         = 234;

        private bool isConnected = false;
        private mode currentMode; // Stores the modes of sort, data, or color modes
        private bool calibrationMode = false; // Is true if we're sending a calibration 
        private int discrim1, discrim2, discrim3, discrimConst, f1, f2, f3, signs, seedSize, checksum;  // Temp variables when sending a calibration
        private Action<char, object> listener; // This holds a reference to a listener method to handle the data received.
        
        /// <summary>
        /// Constructor creates an instance of the SorterComm object
        /// </summary>
        /// <param name="_listener"></param>
        public SorterComm(Action<char, object> _listener)
        {
            listener = _listener;

            findAndConnect();
        }

        /// <summary>
        /// Returns the current listener
        /// </summary>
        public Action<char, object> Listener { get => listener; }
        
        //private static void psudoListener(char c, object listenerData)
        //{
        //    if (listenerData is string)
        //        Console.Out.WriteLine(c + ": " + (string)listenerData);
        //}

        /// <summary>
        /// Logs information. In this case, just 
        /// </summary>
        /// <param name="l"></param>
        private void log(string l)
        {
            listener('i', l);
        }

        /// <summary>
        /// Returns true if device is connected.
        /// </summary>
        public bool IsConnected { get => isConnected; }

        /// <summary>
        /// Intelligently finds the device if it is plugged in.
        /// </summary>
        /// <returns>True if successfully found. False otherwise.</returns>
        private bool findSorter()
        {

            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Caption like '%Serial%(COM%'"))
            {
                var portnames = SerialPort.GetPortNames();
                var ports = searcher.Get().Cast<ManagementBaseObject>().ToList().Select(p => p["Caption"].ToString());

                var portList = portnames.Select(n => ports.FirstOrDefault(s => s.Contains(n))).ToList(); 
                //theres a better way for this, but I dont know what it is yet
                List<string> portList2 = new List<string>();

                foreach (string s in portList)
                {
                    if (s != null)
                    {
                        log(s);
                        portList2.Add(s);
                    }
                }
                
                if (portList2.Count == 0)
                {
                    //no serial ports found
                    System.Windows.Forms.MessageBox.Show("No Device found. Please plug in the USB cable and click Connect.");
                    return false;
                } 
                else if (portList2.Count == 1)
                {
                    //port found
                    port_name = portList2.First().Split('(')[1].Split(')')[0];
                    log("Serial port found on "+ port_name);
                    return true;
                }
                else
                {
                    //more than 1 port found
                    MessageBox.Show("Multiple serial devices found. Please plug in only the sorter USB cable and click Connect.");
                    return false;

                    //TODO: make this offer an option to select the port. OR automagically find the correct port
                }
            }
            return false;
        }

        /// <summary>
        /// Opens a connection to the currently set port name
        /// </summary>
        /// <returns></returns>
        public bool connect()
        {
            try
            {
                comPort.Open();
                log("Connected to Serial Port");
                isConnected = true;
                return true;
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Device found, but unable to connect. Please unplug and replug in the USB cable and restart the interface.");
                isConnected = false;
                return false;
            }
        }

        /// <summary>
        /// Disconnects from the device
        /// </summary>
        public void disconnect()
        {
            if (comPort != null && comPort.IsOpen)
            {
                comPort.Close();
                listener('i', "Disconnected");
            }
            isConnected = false;
        }

        /// <summary>
        /// Finds the connected device and connects
        /// </summary>
        public void findAndConnect()
        {
            if (isConnected)
            {
                listener('i', "Already connected to " + port_name);
            }
            else
            {
                if (findSorter())
                {
                    comPort = new SerialPort(port_name, port_BaudRate, port_Parity, port_DataBits, port_StopBits);
                    comPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                    comPort.ReadTimeout = port_ReadTimeout;
                    comPort.WriteTimeout = port_WriteTimeout;
                    connect();
                    currentMode = mode.Sort;

                }
            }
        }
        
        /// <summary>
        /// Runs when the port receives data from the device
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (isConnected)
            {
                //InputData = comPort.ReadExisting();
                //if (InputData != string.Empty)
                //{
                //    listener('d', InputData);
                //}

                int numBytes = comPort.BytesToRead;
                byte[] buffer = new byte[numBytes];
                comPort.Read(buffer, 0, numBytes);
                if (numBytes > 0)
                {
                    if (calibrationMode)
                    {
                        if (buffer[0] == 223)
                            sendCalibrationPart2();
                        else
                            sendCalibrationPart3(buffer[0]);
                    }
                    else
                    {
                        listener('d', buffer);
                    }
                }
            }
        }
        
        /// <summary>
        /// Sends command to the device
        /// </summary>
        /// <param name="data"></param>
        private void sendCommand(int data)
        {
            sendCommand((byte)data);
        }

        /// <summary>
        /// Sends command to the device
        /// </summary>
        /// <param name="data"></param>
        private void sendCommand(byte data)
        {
            if (isConnected)
            {
                byte[] bytes = { data };
                comPort.Write(bytes, 0, 1);
                System.Threading.Thread.Sleep(30);
            }
        }

        /// <summary>
        /// Sends the reject threshold to the device
        /// </summary>
        /// <param name="rejectThreshold"></param>
        public void sendRejectThreshold(int rejectThreshold)
        {
            sendCommand(rejectThreshold);
        }

        /// <summary>
        /// Change the mode (Sort, Data, colors) of the device
        /// </summary>
        /// <param name="m"></param>
        public void changeModeTo(mode m)
        {
            currentMode = m;
            switch (m)
            {
                case mode.Sort:
                    sendCommand(cmdSORT);
                    break;
                case mode.Data:
                    sendCommand(cmdDATA);
                    break;
                case mode.Blue:
                    sendCommand(cmdBLUE);
                    break;
                case mode.Green:
                    sendCommand(cmdGREEN);
                    break;
                case mode.Red:
                    sendCommand(cmdRED);
                    break;
                case mode.IR850:
                    sendCommand(cmdIR850);
                    break;
                case mode.IR880:
                    sendCommand(cmdIR880);
                    break;
                case mode.IR910:
                    sendCommand(cmdIR910);
                    break;
                case mode.IR940:
                    sendCommand(cmdIR940);
                    break;
                case mode.IR970:
                    sendCommand(cmdIR970);
                    break;
                case mode.IR1070:
                    sendCommand(cmdIR1070);
                    break;
            }
        }
        
        /// <summary>
        /// Starts the process of sending a calibration to the devices
        /// Sends the calibration command and sets CalibrationMode to true, waiting for Go Ahead from device
        /// </summary>
        /// <param name="_discrim1"></param>
        /// <param name="_discrim2"></param>
        /// <param name="_discrim3"></param>
        /// <param name="_discrimConst"></param>
        /// <param name="_f1"></param>
        /// <param name="_f2"></param>
        /// <param name="_f3"></param>
        /// <param name="_signs"></param>
        /// <param name="_seedSize"></param>
        public void sendCalibration(int _discrim1, int _discrim2, int _discrim3, int _discrimConst, int _f1, int _f2, int _f3, int _signs, int _seedSize)
        {
            calibrationMode = true;
            listener('i', "Setting device for calibration");
            discrim1 = _discrim1;
            discrim2 = _discrim2;
            discrim3 = _discrim3;
            discrimConst = _discrimConst;
            f1 = _f1;
            f2 = _f2;
            f3 = _f3;
            signs = _signs;
            seedSize = _seedSize;

            sendCommand(cmdCALIBRATION);
        }

        /// <summary>
        /// The device has sent back the Go Ahead. Sends each byte of the calibration and sends the checksum
        /// </summary>
        private void sendCalibrationPart2()
        {
            checksum = (discrim1 & (byte)15)
                + (discrim2 & (byte)15)
                + (discrim3 & (byte)15)
                + (discrimConst & (byte)15)
                + (f1 & (byte)15)
                + (f2 & (byte)15)
                + (f3 & (byte)15)
                + (signs & (byte)15);

            listener('i', "Device ready for calibration");
            byte b = (byte)discrim1;
            sendCommand(b);
            
            b = (byte)(discrim1 >> 8);
            listener('i', "Sending discrim1 = " + discrim1);
            sendCommand(b);

            b = (byte)discrim2;
            sendCommand(b);
            b = (byte)(discrim2 >> 8);
            listener('i', "Sending discrim2 = " + discrim2);
            sendCommand(b);

            b = (byte)discrim3;
            sendCommand(b);
            b = (byte)(discrim3 >> 8);
            listener('i', "Sending discrim3 = " + discrim3);
            sendCommand(b);

            b = (byte)discrimConst;
            sendCommand(b);
            b = (byte)(discrimConst >> 8);
            listener('i', "Sending discrimConst = " + discrimConst);
            sendCommand(b);

            b = (byte)f1;
            listener('i', "Sending f1 = " + f1);
            sendCommand(b);

            b = (byte)f2;
            listener('i', "Sending f2 = " + f2);
            sendCommand(b);

            b = (byte)f3;
            listener('i', "Sending f3 = " + f3);
            sendCommand(b);

            b = (byte)signs;
            listener('i', "Sending signs = " + signs);
            sendCommand(b);

            b = (byte)seedSize;
            listener('i', "Sending seedSize = " + seedSize);
            listener('i', "Checksum = " + checksum);
            sendCommand(b);

            //sendCommand(checksum);
        }

        /// <summary>
        /// The device has sent back the checksum. This checks it for match.
        /// </summary>
        /// <param name="_checksum"></param>
        private void sendCalibrationPart3(int _checksum)
        {

            listener('i', "Checksum Received = " + _checksum);
            if (_checksum != checksum)
            {
                listener('i', "Calibration NOT loaded correctly. Please try again.");
                MessageBox.Show("Calibration NOT loaded correctly. Please try again.");
            }
            else
            {
                listener('i', "Calibration loaded. Happy Sorting.");
            }
            calibrationMode = false;
        }

        /// <summary>
        /// Cycles the device between the sort, data, and color modes
        /// </summary>
        public void testCommandCycle()
        {
            sendCommand(cmdSORT);
            System.Threading.Thread.Sleep(1000);

            sendCommand(cmdDATA);
            System.Threading.Thread.Sleep(1000);

            sendCommand(cmdBLUE);
            System.Threading.Thread.Sleep(1000);

            sendCommand(cmdGREEN);
            System.Threading.Thread.Sleep(1000);

            sendCommand(cmdRED);
            System.Threading.Thread.Sleep(1000);

            sendCommand(cmdIR850);
            System.Threading.Thread.Sleep(1000);
            sendCommand(cmdIR880);
            System.Threading.Thread.Sleep(1000);
            sendCommand(cmdIR910);
            System.Threading.Thread.Sleep(1000);
            sendCommand(cmdIR940);
            System.Threading.Thread.Sleep(1000);
            sendCommand(cmdIR970);
            System.Threading.Thread.Sleep(1000);
            sendCommand(cmdIR1070);
            System.Threading.Thread.Sleep(1000);
            sendCommand(cmdSORT);
            System.Threading.Thread.Sleep(1000);

        }

        /// <summary>
        /// sends the command to fire the air valve
        /// </summary>
        public void testAir()
        {
            sendCommand(cmdAir);
        }
    }
}
