﻿namespace Volksorter_Interface
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.gbMode = new System.Windows.Forms.GroupBox();
            this.rb1070 = new System.Windows.Forms.RadioButton();
            this.rbIR940 = new System.Windows.Forms.RadioButton();
            this.rbIR970 = new System.Windows.Forms.RadioButton();
            this.rbSort = new System.Windows.Forms.RadioButton();
            this.rbData = new System.Windows.Forms.RadioButton();
            this.rbIR910 = new System.Windows.Forms.RadioButton();
            this.rbBlue = new System.Windows.Forms.RadioButton();
            this.rbGreen = new System.Windows.Forms.RadioButton();
            this.rbIR880 = new System.Windows.Forms.RadioButton();
            this.rbRed = new System.Windows.Forms.RadioButton();
            this.rbIR850 = new System.Windows.Forms.RadioButton();
            this.gbReject = new System.Windows.Forms.GroupBox();
            this.btRejectThresh = new System.Windows.Forms.Button();
            this.nudRejectThresh = new System.Windows.Forms.NumericUpDown();
            this.gbCounts = new System.Windows.Forms.GroupBox();
            this.btResetCounts = new System.Windows.Forms.Button();
            this.lblRejectNum = new System.Windows.Forms.Label();
            this.lblRejects = new System.Windows.Forms.Label();
            this.lblKernelNum = new System.Windows.Forms.Label();
            this.lblKernels = new System.Windows.Forms.Label();
            this.gbLoadCal = new System.Windows.Forms.GroupBox();
            this.lblSelectedCalFile = new System.Windows.Forms.Label();
            this.lblSelectedCalLabel = new System.Windows.Forms.Label();
            this.lblLastCalLoaded = new System.Windows.Forms.Label();
            this.lblLastCalLabel = new System.Windows.Forms.Label();
            this.btCalFileRefresh = new System.Windows.Forms.Button();
            this.btLoadCal = new System.Windows.Forms.Button();
            this.lbCalFileList = new System.Windows.Forms.ListBox();
            this.lblLoadCalDescription = new System.Windows.Forms.Label();
            this.rbCorn = new System.Windows.Forms.RadioButton();
            this.rbBarley = new System.Windows.Forms.RadioButton();
            this.rbDurum = new System.Windows.Forms.RadioButton();
            this.rbWheat = new System.Windows.Forms.RadioButton();
            this.rbFlax = new System.Windows.Forms.RadioButton();
            this.rbAlfalfa = new System.Windows.Forms.RadioButton();
            this.gbCreateCalibration = new System.Windows.Forms.GroupBox();
            this.cbLoadCalAfterCreate = new System.Windows.Forms.CheckBox();
            this.lblSaveCalInfo = new System.Windows.Forms.Label();
            this.btSaveCal = new System.Windows.Forms.Button();
            this.tbCalibrationName = new System.Windows.Forms.TextBox();
            this.rbRejectLight = new System.Windows.Forms.RadioButton();
            this.rbRejectDark = new System.Windows.Forms.RadioButton();
            this.btCreateCalibration = new System.Windows.Forms.Button();
            this.btCloseLight = new System.Windows.Forms.Button();
            this.btCollectLight = new System.Windows.Forms.Button();
            this.btCloseDark = new System.Windows.Forms.Button();
            this.btCollectDark = new System.Windows.Forms.Button();
            this.gbConnect = new System.Windows.Forms.GroupBox();
            this.btTestAir = new System.Windows.Forms.Button();
            this.btDisconnect = new System.Windows.Forms.Button();
            this.btConnect = new System.Windows.Forms.Button();
            this.gbSaveData = new System.Windows.Forms.GroupBox();
            this.lblSampleSizeLabel = new System.Windows.Forms.Label();
            this.nupSampleSize = new System.Windows.Forms.NumericUpDown();
            this.cbIncludeDiscrim = new System.Windows.Forms.CheckBox();
            this.btCloseSample = new System.Windows.Forms.Button();
            this.btCollectSample = new System.Windows.Forms.Button();
            this.tbSaveDataFileName = new System.Windows.Forms.TextBox();
            this.lblSaveDataLabel = new System.Windows.Forms.Label();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblStatusStrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.gbMode.SuspendLayout();
            this.gbReject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRejectThresh)).BeginInit();
            this.gbCounts.SuspendLayout();
            this.gbLoadCal.SuspendLayout();
            this.gbCreateCalibration.SuspendLayout();
            this.gbConnect.SuspendLayout();
            this.gbSaveData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupSampleSize)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbOutput
            // 
            this.tbOutput.AcceptsReturn = true;
            this.tbOutput.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbOutput.ForeColor = System.Drawing.Color.ForestGreen;
            this.tbOutput.Location = new System.Drawing.Point(13, 13);
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbOutput.Size = new System.Drawing.Size(941, 170);
            this.tbOutput.TabIndex = 0;
            // 
            // gbMode
            // 
            this.gbMode.Controls.Add(this.rb1070);
            this.gbMode.Controls.Add(this.rbIR940);
            this.gbMode.Controls.Add(this.rbIR970);
            this.gbMode.Controls.Add(this.rbSort);
            this.gbMode.Controls.Add(this.rbData);
            this.gbMode.Controls.Add(this.rbIR910);
            this.gbMode.Controls.Add(this.rbBlue);
            this.gbMode.Controls.Add(this.rbGreen);
            this.gbMode.Controls.Add(this.rbIR880);
            this.gbMode.Controls.Add(this.rbRed);
            this.gbMode.Controls.Add(this.rbIR850);
            this.gbMode.Location = new System.Drawing.Point(138, 189);
            this.gbMode.Name = "gbMode";
            this.gbMode.Size = new System.Drawing.Size(78, 289);
            this.gbMode.TabIndex = 4;
            this.gbMode.TabStop = false;
            this.gbMode.Text = "Mode";
            this.gbMode.Paint += new System.Windows.Forms.PaintEventHandler(this.groupbox_paint);
            // 
            // rb1070
            // 
            this.rb1070.AutoSize = true;
            this.rb1070.Location = new System.Drawing.Point(6, 249);
            this.rb1070.Name = "rb1070";
            this.rb1070.Size = new System.Drawing.Size(60, 17);
            this.rb1070.TabIndex = 3;
            this.rb1070.Text = "IR1070";
            this.rb1070.UseVisualStyleBackColor = true;
            this.rb1070.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbIR940
            // 
            this.rbIR940.AutoSize = true;
            this.rbIR940.Location = new System.Drawing.Point(6, 203);
            this.rbIR940.Name = "rbIR940";
            this.rbIR940.Size = new System.Drawing.Size(54, 17);
            this.rbIR940.TabIndex = 8;
            this.rbIR940.Text = "IR940";
            this.rbIR940.UseVisualStyleBackColor = true;
            this.rbIR940.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbIR970
            // 
            this.rbIR970.AutoSize = true;
            this.rbIR970.Location = new System.Drawing.Point(6, 226);
            this.rbIR970.Name = "rbIR970";
            this.rbIR970.Size = new System.Drawing.Size(54, 17);
            this.rbIR970.TabIndex = 2;
            this.rbIR970.Text = "IR970";
            this.rbIR970.UseVisualStyleBackColor = true;
            this.rbIR970.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbSort
            // 
            this.rbSort.AutoSize = true;
            this.rbSort.Checked = true;
            this.rbSort.Location = new System.Drawing.Point(6, 19);
            this.rbSort.Name = "rbSort";
            this.rbSort.Size = new System.Drawing.Size(44, 17);
            this.rbSort.TabIndex = 0;
            this.rbSort.TabStop = true;
            this.rbSort.Text = "Sort";
            this.rbSort.UseVisualStyleBackColor = true;
            this.rbSort.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbData
            // 
            this.rbData.AutoSize = true;
            this.rbData.Location = new System.Drawing.Point(6, 42);
            this.rbData.Name = "rbData";
            this.rbData.Size = new System.Drawing.Size(48, 17);
            this.rbData.TabIndex = 1;
            this.rbData.Text = "Data";
            this.rbData.UseVisualStyleBackColor = true;
            this.rbData.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbIR910
            // 
            this.rbIR910.AutoSize = true;
            this.rbIR910.Location = new System.Drawing.Point(6, 180);
            this.rbIR910.Name = "rbIR910";
            this.rbIR910.Size = new System.Drawing.Size(54, 17);
            this.rbIR910.TabIndex = 7;
            this.rbIR910.Text = "IR910";
            this.rbIR910.UseVisualStyleBackColor = true;
            this.rbIR910.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbBlue
            // 
            this.rbBlue.AutoSize = true;
            this.rbBlue.Location = new System.Drawing.Point(6, 65);
            this.rbBlue.Name = "rbBlue";
            this.rbBlue.Size = new System.Drawing.Size(46, 17);
            this.rbBlue.TabIndex = 2;
            this.rbBlue.Text = "Blue";
            this.rbBlue.UseVisualStyleBackColor = true;
            this.rbBlue.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbGreen
            // 
            this.rbGreen.AutoSize = true;
            this.rbGreen.Location = new System.Drawing.Point(6, 88);
            this.rbGreen.Name = "rbGreen";
            this.rbGreen.Size = new System.Drawing.Size(54, 17);
            this.rbGreen.TabIndex = 3;
            this.rbGreen.Text = "Green";
            this.rbGreen.UseVisualStyleBackColor = true;
            this.rbGreen.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbIR880
            // 
            this.rbIR880.AutoSize = true;
            this.rbIR880.Location = new System.Drawing.Point(6, 157);
            this.rbIR880.Name = "rbIR880";
            this.rbIR880.Size = new System.Drawing.Size(54, 17);
            this.rbIR880.TabIndex = 6;
            this.rbIR880.Text = "IR880";
            this.rbIR880.UseVisualStyleBackColor = true;
            this.rbIR880.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbRed
            // 
            this.rbRed.AutoSize = true;
            this.rbRed.Location = new System.Drawing.Point(6, 111);
            this.rbRed.Name = "rbRed";
            this.rbRed.Size = new System.Drawing.Size(45, 17);
            this.rbRed.TabIndex = 4;
            this.rbRed.Text = "Red";
            this.rbRed.UseVisualStyleBackColor = true;
            this.rbRed.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // rbIR850
            // 
            this.rbIR850.AutoSize = true;
            this.rbIR850.Location = new System.Drawing.Point(6, 134);
            this.rbIR850.Name = "rbIR850";
            this.rbIR850.Size = new System.Drawing.Size(54, 17);
            this.rbIR850.TabIndex = 5;
            this.rbIR850.Text = "IR850";
            this.rbIR850.UseVisualStyleBackColor = true;
            this.rbIR850.CheckedChanged += new System.EventHandler(this.modeChanged);
            // 
            // gbReject
            // 
            this.gbReject.Controls.Add(this.btRejectThresh);
            this.gbReject.Controls.Add(this.nudRejectThresh);
            this.gbReject.Location = new System.Drawing.Point(12, 310);
            this.gbReject.Name = "gbReject";
            this.gbReject.Size = new System.Drawing.Size(120, 58);
            this.gbReject.TabIndex = 2;
            this.gbReject.TabStop = false;
            this.gbReject.Text = "Reject Threshold";
            this.gbReject.Paint += new System.Windows.Forms.PaintEventHandler(this.groupbox_paint);
            // 
            // btRejectThresh
            // 
            this.btRejectThresh.Location = new System.Drawing.Point(62, 21);
            this.btRejectThresh.Name = "btRejectThresh";
            this.btRejectThresh.Size = new System.Drawing.Size(49, 23);
            this.btRejectThresh.TabIndex = 1;
            this.btRejectThresh.Text = "Send";
            this.btRejectThresh.UseVisualStyleBackColor = true;
            this.btRejectThresh.Click += new System.EventHandler(this.btRejectThresh_Click);
            // 
            // nudRejectThresh
            // 
            this.nudRejectThresh.Location = new System.Drawing.Point(7, 23);
            this.nudRejectThresh.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudRejectThresh.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRejectThresh.Name = "nudRejectThresh";
            this.nudRejectThresh.Size = new System.Drawing.Size(49, 20);
            this.nudRejectThresh.TabIndex = 0;
            this.nudRejectThresh.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // gbCounts
            // 
            this.gbCounts.Controls.Add(this.btResetCounts);
            this.gbCounts.Controls.Add(this.lblRejectNum);
            this.gbCounts.Controls.Add(this.lblRejects);
            this.gbCounts.Controls.Add(this.lblKernelNum);
            this.gbCounts.Controls.Add(this.lblKernels);
            this.gbCounts.Location = new System.Drawing.Point(11, 378);
            this.gbCounts.Name = "gbCounts";
            this.gbCounts.Size = new System.Drawing.Size(121, 100);
            this.gbCounts.TabIndex = 3;
            this.gbCounts.TabStop = false;
            this.gbCounts.Text = "Counts";
            this.gbCounts.Paint += new System.Windows.Forms.PaintEventHandler(this.groupbox_paint);
            // 
            // btResetCounts
            // 
            this.btResetCounts.Location = new System.Drawing.Point(10, 69);
            this.btResetCounts.Name = "btResetCounts";
            this.btResetCounts.Size = new System.Drawing.Size(101, 23);
            this.btResetCounts.TabIndex = 1;
            this.btResetCounts.Text = "Reset Counts";
            this.btResetCounts.UseVisualStyleBackColor = true;
            this.btResetCounts.Click += new System.EventHandler(this.btResetCounts_Click);
            // 
            // lblRejectNum
            // 
            this.lblRejectNum.AutoSize = true;
            this.lblRejectNum.Location = new System.Drawing.Point(56, 37);
            this.lblRejectNum.Name = "lblRejectNum";
            this.lblRejectNum.Size = new System.Drawing.Size(13, 13);
            this.lblRejectNum.TabIndex = 3;
            this.lblRejectNum.Text = "0";
            // 
            // lblRejects
            // 
            this.lblRejects.AutoSize = true;
            this.lblRejects.Location = new System.Drawing.Point(7, 37);
            this.lblRejects.Name = "lblRejects";
            this.lblRejects.Size = new System.Drawing.Size(43, 13);
            this.lblRejects.TabIndex = 2;
            this.lblRejects.Text = "Rejects";
            // 
            // lblKernelNum
            // 
            this.lblKernelNum.AutoSize = true;
            this.lblKernelNum.Location = new System.Drawing.Point(56, 20);
            this.lblKernelNum.Name = "lblKernelNum";
            this.lblKernelNum.Size = new System.Drawing.Size(13, 13);
            this.lblKernelNum.TabIndex = 1;
            this.lblKernelNum.Text = "0";
            // 
            // lblKernels
            // 
            this.lblKernels.AutoSize = true;
            this.lblKernels.Location = new System.Drawing.Point(7, 20);
            this.lblKernels.Name = "lblKernels";
            this.lblKernels.Size = new System.Drawing.Size(42, 13);
            this.lblKernels.TabIndex = 0;
            this.lblKernels.Text = "Kernels";
            // 
            // gbLoadCal
            // 
            this.gbLoadCal.Controls.Add(this.lblSelectedCalFile);
            this.gbLoadCal.Controls.Add(this.lblSelectedCalLabel);
            this.gbLoadCal.Controls.Add(this.lblLastCalLoaded);
            this.gbLoadCal.Controls.Add(this.lblLastCalLabel);
            this.gbLoadCal.Controls.Add(this.btCalFileRefresh);
            this.gbLoadCal.Controls.Add(this.btLoadCal);
            this.gbLoadCal.Controls.Add(this.lbCalFileList);
            this.gbLoadCal.Controls.Add(this.lblLoadCalDescription);
            this.gbLoadCal.Controls.Add(this.rbCorn);
            this.gbLoadCal.Controls.Add(this.rbBarley);
            this.gbLoadCal.Controls.Add(this.rbDurum);
            this.gbLoadCal.Controls.Add(this.rbWheat);
            this.gbLoadCal.Controls.Add(this.rbFlax);
            this.gbLoadCal.Controls.Add(this.rbAlfalfa);
            this.gbLoadCal.Location = new System.Drawing.Point(503, 189);
            this.gbLoadCal.Name = "gbLoadCal";
            this.gbLoadCal.Size = new System.Drawing.Size(257, 289);
            this.gbLoadCal.TabIndex = 6;
            this.gbLoadCal.TabStop = false;
            this.gbLoadCal.Text = "Load Calibration";
            this.gbLoadCal.Paint += new System.Windows.Forms.PaintEventHandler(this.groupbox_paint);
            // 
            // lblSelectedCalFile
            // 
            this.lblSelectedCalFile.AutoSize = true;
            this.lblSelectedCalFile.Location = new System.Drawing.Point(133, 217);
            this.lblSelectedCalFile.Name = "lblSelectedCalFile";
            this.lblSelectedCalFile.Size = new System.Drawing.Size(16, 13);
            this.lblSelectedCalFile.TabIndex = 13;
            this.lblSelectedCalFile.Text = "...";
            // 
            // lblSelectedCalLabel
            // 
            this.lblSelectedCalLabel.AutoSize = true;
            this.lblSelectedCalLabel.Location = new System.Drawing.Point(7, 217);
            this.lblSelectedCalLabel.Name = "lblSelectedCalLabel";
            this.lblSelectedCalLabel.Size = new System.Drawing.Size(120, 13);
            this.lblSelectedCalLabel.TabIndex = 12;
            this.lblSelectedCalLabel.Text = "Selected Calibration File";
            // 
            // lblLastCalLoaded
            // 
            this.lblLastCalLoaded.AutoSize = true;
            this.lblLastCalLoaded.Location = new System.Drawing.Point(133, 232);
            this.lblLastCalLoaded.Name = "lblLastCalLoaded";
            this.lblLastCalLoaded.Size = new System.Drawing.Size(16, 13);
            this.lblLastCalLoaded.TabIndex = 11;
            this.lblLastCalLoaded.Text = "...";
            // 
            // lblLastCalLabel
            // 
            this.lblLastCalLabel.AutoSize = true;
            this.lblLastCalLabel.Location = new System.Drawing.Point(7, 232);
            this.lblLastCalLabel.Name = "lblLastCalLabel";
            this.lblLastCalLabel.Size = new System.Drawing.Size(118, 13);
            this.lblLastCalLabel.TabIndex = 10;
            this.lblLastCalLabel.Text = "Last Calibration Loaded";
            // 
            // btCalFileRefresh
            // 
            this.btCalFileRefresh.Location = new System.Drawing.Point(114, 184);
            this.btCalFileRefresh.Name = "btCalFileRefresh";
            this.btCalFileRefresh.Size = new System.Drawing.Size(133, 23);
            this.btCalFileRefresh.TabIndex = 9;
            this.btCalFileRefresh.Text = "Refresh List";
            this.btCalFileRefresh.UseVisualStyleBackColor = true;
            this.btCalFileRefresh.Click += new System.EventHandler(this.btCalFileRefresh_Click);
            // 
            // btLoadCal
            // 
            this.btLoadCal.Location = new System.Drawing.Point(6, 260);
            this.btLoadCal.Name = "btLoadCal";
            this.btLoadCal.Size = new System.Drawing.Size(237, 23);
            this.btLoadCal.TabIndex = 8;
            this.btLoadCal.Text = "Load Calibration";
            this.btLoadCal.UseVisualStyleBackColor = true;
            this.btLoadCal.Click += new System.EventHandler(this.btLoadCal_Click);
            // 
            // lbCalFileList
            // 
            this.lbCalFileList.FormattingEnabled = true;
            this.lbCalFileList.Location = new System.Drawing.Point(115, 50);
            this.lbCalFileList.Name = "lbCalFileList";
            this.lbCalFileList.Size = new System.Drawing.Size(132, 134);
            this.lbCalFileList.TabIndex = 7;
            this.lbCalFileList.SelectedIndexChanged += new System.EventHandler(this.lbCalFileList_SelectedIndexChanged);
            // 
            // lblLoadCalDescription
            // 
            this.lblLoadCalDescription.AutoSize = true;
            this.lblLoadCalDescription.Location = new System.Drawing.Point(7, 19);
            this.lblLoadCalDescription.Name = "lblLoadCalDescription";
            this.lblLoadCalDescription.Size = new System.Drawing.Size(227, 26);
            this.lblLoadCalDescription.TabIndex = 6;
            this.lblLoadCalDescription.Text = "The last calibration file generated will be used. \r\nSelect a different file if de" +
    "sired.";
            // 
            // rbCorn
            // 
            this.rbCorn.AutoSize = true;
            this.rbCorn.Location = new System.Drawing.Point(6, 169);
            this.rbCorn.Name = "rbCorn";
            this.rbCorn.Size = new System.Drawing.Size(93, 17);
            this.rbCorn.TabIndex = 5;
            this.rbCorn.Text = "15.0mm - Corn";
            this.rbCorn.UseVisualStyleBackColor = true;
            // 
            // rbBarley
            // 
            this.rbBarley.AutoSize = true;
            this.rbBarley.Location = new System.Drawing.Point(6, 146);
            this.rbBarley.Name = "rbBarley";
            this.rbBarley.Size = new System.Drawing.Size(100, 17);
            this.rbBarley.TabIndex = 4;
            this.rbBarley.Text = "12.0mm - Barley";
            this.rbBarley.UseVisualStyleBackColor = true;
            // 
            // rbDurum
            // 
            this.rbDurum.AutoSize = true;
            this.rbDurum.Location = new System.Drawing.Point(6, 123);
            this.rbDurum.Name = "rbDurum";
            this.rbDurum.Size = new System.Drawing.Size(96, 17);
            this.rbDurum.TabIndex = 3;
            this.rbDurum.Text = "9.0mm - Durum";
            this.rbDurum.UseVisualStyleBackColor = true;
            // 
            // rbWheat
            // 
            this.rbWheat.AutoSize = true;
            this.rbWheat.Checked = true;
            this.rbWheat.Location = new System.Drawing.Point(6, 100);
            this.rbWheat.Name = "rbWheat";
            this.rbWheat.Size = new System.Drawing.Size(97, 17);
            this.rbWheat.TabIndex = 2;
            this.rbWheat.TabStop = true;
            this.rbWheat.Text = "7.0mm - Wheat";
            this.rbWheat.UseVisualStyleBackColor = true;
            // 
            // rbFlax
            // 
            this.rbFlax.AutoSize = true;
            this.rbFlax.Location = new System.Drawing.Point(6, 77);
            this.rbFlax.Name = "rbFlax";
            this.rbFlax.Size = new System.Drawing.Size(84, 17);
            this.rbFlax.TabIndex = 1;
            this.rbFlax.Text = "5.5mm - Flax";
            this.rbFlax.UseVisualStyleBackColor = true;
            // 
            // rbAlfalfa
            // 
            this.rbAlfalfa.AutoSize = true;
            this.rbAlfalfa.Location = new System.Drawing.Point(6, 54);
            this.rbAlfalfa.Name = "rbAlfalfa";
            this.rbAlfalfa.Size = new System.Drawing.Size(94, 17);
            this.rbAlfalfa.TabIndex = 0;
            this.rbAlfalfa.Text = "3.0mm - Alfalfa";
            this.rbAlfalfa.UseVisualStyleBackColor = true;
            // 
            // gbCreateCalibration
            // 
            this.gbCreateCalibration.Controls.Add(this.cbLoadCalAfterCreate);
            this.gbCreateCalibration.Controls.Add(this.lblSaveCalInfo);
            this.gbCreateCalibration.Controls.Add(this.btSaveCal);
            this.gbCreateCalibration.Controls.Add(this.tbCalibrationName);
            this.gbCreateCalibration.Controls.Add(this.rbRejectLight);
            this.gbCreateCalibration.Controls.Add(this.rbRejectDark);
            this.gbCreateCalibration.Controls.Add(this.btCreateCalibration);
            this.gbCreateCalibration.Controls.Add(this.btCloseLight);
            this.gbCreateCalibration.Controls.Add(this.btCollectLight);
            this.gbCreateCalibration.Controls.Add(this.btCloseDark);
            this.gbCreateCalibration.Controls.Add(this.btCollectDark);
            this.gbCreateCalibration.Location = new System.Drawing.Point(272, 189);
            this.gbCreateCalibration.Name = "gbCreateCalibration";
            this.gbCreateCalibration.Size = new System.Drawing.Size(178, 289);
            this.gbCreateCalibration.TabIndex = 5;
            this.gbCreateCalibration.TabStop = false;
            this.gbCreateCalibration.Text = "Create New Calibration";
            this.gbCreateCalibration.Paint += new System.Windows.Forms.PaintEventHandler(this.groupbox_paint);
            // 
            // cbLoadCalAfterCreate
            // 
            this.cbLoadCalAfterCreate.AutoSize = true;
            this.cbLoadCalAfterCreate.Checked = true;
            this.cbLoadCalAfterCreate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLoadCalAfterCreate.Location = new System.Drawing.Point(6, 141);
            this.cbLoadCalAfterCreate.Name = "cbLoadCalAfterCreate";
            this.cbLoadCalAfterCreate.Size = new System.Drawing.Size(166, 17);
            this.cbLoadCalAfterCreate.TabIndex = 6;
            this.cbLoadCalAfterCreate.Text = "Load calibration after creation";
            this.cbLoadCalAfterCreate.UseVisualStyleBackColor = true;
            // 
            // lblSaveCalInfo
            // 
            this.lblSaveCalInfo.AutoSize = true;
            this.lblSaveCalInfo.Location = new System.Drawing.Point(6, 197);
            this.lblSaveCalInfo.Name = "lblSaveCalInfo";
            this.lblSaveCalInfo.Size = new System.Drawing.Size(164, 39);
            this.lblSaveCalInfo.TabIndex = 9;
            this.lblSaveCalInfo.Text = "Calibration files are saved as \r\n\"function.cal.\" You can resave it \r\nas another f" +
    "ile if you wish.";
            // 
            // btSaveCal
            // 
            this.btSaveCal.Location = new System.Drawing.Point(6, 260);
            this.btSaveCal.Name = "btSaveCal";
            this.btSaveCal.Size = new System.Drawing.Size(164, 23);
            this.btSaveCal.TabIndex = 9;
            this.btSaveCal.Text = "Save";
            this.btSaveCal.UseVisualStyleBackColor = true;
            this.btSaveCal.Click += new System.EventHandler(this.btSaveCal_Click);
            // 
            // tbCalibrationName
            // 
            this.tbCalibrationName.Location = new System.Drawing.Point(7, 239);
            this.tbCalibrationName.Name = "tbCalibrationName";
            this.tbCalibrationName.Size = new System.Drawing.Size(163, 20);
            this.tbCalibrationName.TabIndex = 8;
            this.tbCalibrationName.Text = "function.cal";
            // 
            // rbRejectLight
            // 
            this.rbRejectLight.AutoSize = true;
            this.rbRejectLight.Location = new System.Drawing.Point(92, 120);
            this.rbRejectLight.Name = "rbRejectLight";
            this.rbRejectLight.Size = new System.Drawing.Size(78, 17);
            this.rbRejectLight.TabIndex = 5;
            this.rbRejectLight.Text = "Reject light";
            this.rbRejectLight.UseVisualStyleBackColor = true;
            // 
            // rbRejectDark
            // 
            this.rbRejectDark.AutoSize = true;
            this.rbRejectDark.Checked = true;
            this.rbRejectDark.Location = new System.Drawing.Point(6, 120);
            this.rbRejectDark.Name = "rbRejectDark";
            this.rbRejectDark.Size = new System.Drawing.Size(80, 17);
            this.rbRejectDark.TabIndex = 4;
            this.rbRejectDark.TabStop = true;
            this.rbRejectDark.Text = "Reject dark";
            this.rbRejectDark.UseVisualStyleBackColor = true;
            // 
            // btCreateCalibration
            // 
            this.btCreateCalibration.Location = new System.Drawing.Point(6, 164);
            this.btCreateCalibration.Name = "btCreateCalibration";
            this.btCreateCalibration.Size = new System.Drawing.Size(164, 24);
            this.btCreateCalibration.TabIndex = 7;
            this.btCreateCalibration.Text = "Create Calibration";
            this.btCreateCalibration.UseVisualStyleBackColor = true;
            this.btCreateCalibration.Click += new System.EventHandler(this.btCreateCalibration_Click);
            // 
            // btCloseLight
            // 
            this.btCloseLight.Enabled = false;
            this.btCloseLight.Location = new System.Drawing.Point(7, 88);
            this.btCloseLight.Name = "btCloseLight";
            this.btCloseLight.Size = new System.Drawing.Size(163, 23);
            this.btCloseLight.TabIndex = 3;
            this.btCloseLight.Text = "Close data file";
            this.btCloseLight.UseVisualStyleBackColor = true;
            this.btCloseLight.Click += new System.EventHandler(this.btCloseLight_Click);
            // 
            // btCollectLight
            // 
            this.btCollectLight.Enabled = false;
            this.btCollectLight.Location = new System.Drawing.Point(7, 65);
            this.btCollectLight.Name = "btCollectLight";
            this.btCollectLight.Size = new System.Drawing.Size(163, 23);
            this.btCollectLight.TabIndex = 2;
            this.btCollectLight.Text = "Collect light seed data";
            this.btCollectLight.UseVisualStyleBackColor = true;
            this.btCollectLight.Click += new System.EventHandler(this.btCollectLight_Click);
            // 
            // btCloseDark
            // 
            this.btCloseDark.Enabled = false;
            this.btCloseDark.Location = new System.Drawing.Point(7, 42);
            this.btCloseDark.Name = "btCloseDark";
            this.btCloseDark.Size = new System.Drawing.Size(163, 23);
            this.btCloseDark.TabIndex = 1;
            this.btCloseDark.Text = "Close data file";
            this.btCloseDark.UseVisualStyleBackColor = true;
            this.btCloseDark.Click += new System.EventHandler(this.btCloseDark_Click);
            // 
            // btCollectDark
            // 
            this.btCollectDark.Enabled = false;
            this.btCollectDark.Location = new System.Drawing.Point(7, 20);
            this.btCollectDark.Name = "btCollectDark";
            this.btCollectDark.Size = new System.Drawing.Size(163, 23);
            this.btCollectDark.TabIndex = 0;
            this.btCollectDark.Text = "Collect dark seed data";
            this.btCollectDark.UseVisualStyleBackColor = true;
            this.btCollectDark.Click += new System.EventHandler(this.btCollectDark_Click);
            // 
            // gbConnect
            // 
            this.gbConnect.Controls.Add(this.btTestAir);
            this.gbConnect.Controls.Add(this.btDisconnect);
            this.gbConnect.Controls.Add(this.btConnect);
            this.gbConnect.Location = new System.Drawing.Point(12, 189);
            this.gbConnect.Name = "gbConnect";
            this.gbConnect.Size = new System.Drawing.Size(120, 110);
            this.gbConnect.TabIndex = 1;
            this.gbConnect.TabStop = false;
            this.gbConnect.Text = "Connection";
            this.gbConnect.Paint += new System.Windows.Forms.PaintEventHandler(this.groupbox_paint);
            // 
            // btTestAir
            // 
            this.btTestAir.Location = new System.Drawing.Point(7, 77);
            this.btTestAir.Name = "btTestAir";
            this.btTestAir.Size = new System.Drawing.Size(104, 23);
            this.btTestAir.TabIndex = 2;
            this.btTestAir.Text = "Test Air Valve";
            this.btTestAir.UseVisualStyleBackColor = true;
            this.btTestAir.Click += new System.EventHandler(this.btTestAir_Click);
            // 
            // btDisconnect
            // 
            this.btDisconnect.Location = new System.Drawing.Point(7, 48);
            this.btDisconnect.Name = "btDisconnect";
            this.btDisconnect.Size = new System.Drawing.Size(104, 23);
            this.btDisconnect.TabIndex = 1;
            this.btDisconnect.Text = "Disconnect";
            this.btDisconnect.UseVisualStyleBackColor = true;
            this.btDisconnect.Click += new System.EventHandler(this.btDisconnect_Click);
            // 
            // btConnect
            // 
            this.btConnect.Location = new System.Drawing.Point(7, 19);
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(104, 23);
            this.btConnect.TabIndex = 0;
            this.btConnect.Text = "Connect";
            this.btConnect.UseVisualStyleBackColor = true;
            this.btConnect.Click += new System.EventHandler(this.btConnect_Click);
            // 
            // gbSaveData
            // 
            this.gbSaveData.Controls.Add(this.lblSampleSizeLabel);
            this.gbSaveData.Controls.Add(this.nupSampleSize);
            this.gbSaveData.Controls.Add(this.cbIncludeDiscrim);
            this.gbSaveData.Controls.Add(this.btCloseSample);
            this.gbSaveData.Controls.Add(this.btCollectSample);
            this.gbSaveData.Controls.Add(this.tbSaveDataFileName);
            this.gbSaveData.Controls.Add(this.lblSaveDataLabel);
            this.gbSaveData.Location = new System.Drawing.Point(815, 189);
            this.gbSaveData.Name = "gbSaveData";
            this.gbSaveData.Size = new System.Drawing.Size(139, 289);
            this.gbSaveData.TabIndex = 7;
            this.gbSaveData.TabStop = false;
            this.gbSaveData.Text = "Save Data";
            this.gbSaveData.Paint += new System.Windows.Forms.PaintEventHandler(this.groupbox_paint);
            // 
            // lblSampleSizeLabel
            // 
            this.lblSampleSizeLabel.AutoSize = true;
            this.lblSampleSizeLabel.Location = new System.Drawing.Point(7, 85);
            this.lblSampleSizeLabel.Name = "lblSampleSizeLabel";
            this.lblSampleSizeLabel.Size = new System.Drawing.Size(63, 13);
            this.lblSampleSizeLabel.TabIndex = 7;
            this.lblSampleSizeLabel.Text = "Sample size";
            // 
            // nupSampleSize
            // 
            this.nupSampleSize.Location = new System.Drawing.Point(76, 83);
            this.nupSampleSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nupSampleSize.Name = "nupSampleSize";
            this.nupSampleSize.Size = new System.Drawing.Size(51, 20);
            this.nupSampleSize.TabIndex = 2;
            this.nupSampleSize.Value = new decimal(new int[] {
            201,
            0,
            0,
            0});
            // 
            // cbIncludeDiscrim
            // 
            this.cbIncludeDiscrim.AutoSize = true;
            this.cbIncludeDiscrim.Location = new System.Drawing.Point(10, 108);
            this.cbIncludeDiscrim.Name = "cbIncludeDiscrim";
            this.cbIncludeDiscrim.Size = new System.Drawing.Size(86, 17);
            this.cbIncludeDiscrim.TabIndex = 3;
            this.cbIncludeDiscrim.Text = "Discrim Data";
            this.cbIncludeDiscrim.UseVisualStyleBackColor = true;
            // 
            // btCloseSample
            // 
            this.btCloseSample.Enabled = false;
            this.btCloseSample.Location = new System.Drawing.Point(6, 156);
            this.btCloseSample.Name = "btCloseSample";
            this.btCloseSample.Size = new System.Drawing.Size(122, 23);
            this.btCloseSample.TabIndex = 5;
            this.btCloseSample.Text = "Close data file";
            this.btCloseSample.UseVisualStyleBackColor = true;
            this.btCloseSample.Click += new System.EventHandler(this.btCloseSample_Click);
            // 
            // btCollectSample
            // 
            this.btCollectSample.Enabled = false;
            this.btCollectSample.Location = new System.Drawing.Point(6, 132);
            this.btCollectSample.Name = "btCollectSample";
            this.btCollectSample.Size = new System.Drawing.Size(122, 23);
            this.btCollectSample.TabIndex = 4;
            this.btCollectSample.Text = "Collect seed data";
            this.btCollectSample.UseVisualStyleBackColor = true;
            this.btCollectSample.Click += new System.EventHandler(this.btCollectSample_Click);
            // 
            // tbSaveDataFileName
            // 
            this.tbSaveDataFileName.Location = new System.Drawing.Point(6, 59);
            this.tbSaveDataFileName.Name = "tbSaveDataFileName";
            this.tbSaveDataFileName.Size = new System.Drawing.Size(122, 20);
            this.tbSaveDataFileName.TabIndex = 1;
            this.tbSaveDataFileName.Text = "data.csv";
            // 
            // lblSaveDataLabel
            // 
            this.lblSaveDataLabel.AutoSize = true;
            this.lblSaveDataLabel.Location = new System.Drawing.Point(7, 17);
            this.lblSaveDataLabel.Name = "lblSaveDataLabel";
            this.lblSaveDataLabel.Size = new System.Drawing.Size(130, 39);
            this.lblSaveDataLabel.TabIndex = 0;
            this.lblSaveDataLabel.Text = "If you wish, you can save \r\ndata from Data mode into \r\nthe file specified.\r\n";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusStrip});
            this.statusStrip.Location = new System.Drawing.Point(0, 481);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(966, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 8;
            this.statusStrip.Text = "statusStrip";
            // 
            // lblStatusStrip
            // 
            this.lblStatusStrip.Name = "lblStatusStrip";
            this.lblStatusStrip.Size = new System.Drawing.Size(626, 17);
            this.lblStatusStrip.Text = "NOTE: Color data modes require firmware version WG2018-1-a or newer. Test Air Val" +
    "ve requires WG2018-1-b or newer.";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 503);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.gbSaveData);
            this.Controls.Add(this.gbConnect);
            this.Controls.Add(this.gbCreateCalibration);
            this.Controls.Add(this.gbLoadCal);
            this.Controls.Add(this.gbCounts);
            this.Controls.Add(this.gbReject);
            this.Controls.Add(this.gbMode);
            this.Controls.Add(this.tbOutput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "mainForm";
            this.Text = "VolkSorter Interface WG2018-1-d (RC)";
            this.gbMode.ResumeLayout(false);
            this.gbMode.PerformLayout();
            this.gbReject.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudRejectThresh)).EndInit();
            this.gbCounts.ResumeLayout(false);
            this.gbCounts.PerformLayout();
            this.gbLoadCal.ResumeLayout(false);
            this.gbLoadCal.PerformLayout();
            this.gbCreateCalibration.ResumeLayout(false);
            this.gbCreateCalibration.PerformLayout();
            this.gbConnect.ResumeLayout(false);
            this.gbSaveData.ResumeLayout(false);
            this.gbSaveData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupSampleSize)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.GroupBox gbMode;
        private System.Windows.Forms.RadioButton rbIR940;
        private System.Windows.Forms.RadioButton rbIR910;
        private System.Windows.Forms.RadioButton rbIR880;
        private System.Windows.Forms.RadioButton rbIR850;
        private System.Windows.Forms.RadioButton rbRed;
        private System.Windows.Forms.RadioButton rbGreen;
        private System.Windows.Forms.RadioButton rbBlue;
        private System.Windows.Forms.RadioButton rbData;
        private System.Windows.Forms.RadioButton rbSort;
        private System.Windows.Forms.RadioButton rb1070;
        private System.Windows.Forms.RadioButton rbIR970;
        private System.Windows.Forms.GroupBox gbReject;
        private System.Windows.Forms.Button btRejectThresh;
        private System.Windows.Forms.NumericUpDown nudRejectThresh;
        private System.Windows.Forms.GroupBox gbCounts;
        private System.Windows.Forms.Button btResetCounts;
        private System.Windows.Forms.Label lblRejectNum;
        private System.Windows.Forms.Label lblRejects;
        private System.Windows.Forms.Label lblKernelNum;
        private System.Windows.Forms.Label lblKernels;
        private System.Windows.Forms.GroupBox gbLoadCal;
        private System.Windows.Forms.Button btLoadCal;
        private System.Windows.Forms.ListBox lbCalFileList;
        private System.Windows.Forms.Label lblLoadCalDescription;
        private System.Windows.Forms.RadioButton rbCorn;
        private System.Windows.Forms.RadioButton rbBarley;
        private System.Windows.Forms.RadioButton rbDurum;
        private System.Windows.Forms.RadioButton rbWheat;
        private System.Windows.Forms.RadioButton rbFlax;
        private System.Windows.Forms.RadioButton rbAlfalfa;
        private System.Windows.Forms.Label lblSelectedCalFile;
        private System.Windows.Forms.Label lblSelectedCalLabel;
        private System.Windows.Forms.Label lblLastCalLoaded;
        private System.Windows.Forms.Label lblLastCalLabel;
        private System.Windows.Forms.Button btCalFileRefresh;
        private System.Windows.Forms.GroupBox gbCreateCalibration;
        private System.Windows.Forms.Button btSaveCal;
        private System.Windows.Forms.TextBox tbCalibrationName;
        private System.Windows.Forms.RadioButton rbRejectLight;
        private System.Windows.Forms.RadioButton rbRejectDark;
        private System.Windows.Forms.Button btCreateCalibration;
        private System.Windows.Forms.Button btCloseLight;
        private System.Windows.Forms.Button btCollectLight;
        private System.Windows.Forms.Button btCloseDark;
        private System.Windows.Forms.Button btCollectDark;
        private System.Windows.Forms.Label lblSaveCalInfo;
        private System.Windows.Forms.GroupBox gbConnect;
        private System.Windows.Forms.Button btDisconnect;
        private System.Windows.Forms.Button btConnect;
        private System.Windows.Forms.GroupBox gbSaveData;
        private System.Windows.Forms.Button btCloseSample;
        private System.Windows.Forms.Button btCollectSample;
        private System.Windows.Forms.TextBox tbSaveDataFileName;
        private System.Windows.Forms.Label lblSaveDataLabel;
        private System.Windows.Forms.CheckBox cbLoadCalAfterCreate;
        private System.Windows.Forms.Label lblSampleSizeLabel;
        private System.Windows.Forms.NumericUpDown nupSampleSize;
        private System.Windows.Forms.CheckBox cbIncludeDiscrim;
        private System.Windows.Forms.Button btTestAir;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusStrip;
    }
}

